
import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;


public class Linea extends Figura {
    Point2D punto1;
    Point2D punto2;

    Linea(Point p1Linea, Point p2Linea, Color colorLinea) {
        punto1 = p1Linea;
        punto2 = p2Linea;
        color  = colorLinea;
    }    
}
