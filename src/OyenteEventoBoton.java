
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import javax.swing.JButton;



public class OyenteEventoBoton implements ActionListener {

    private Pizarra panel;

    public OyenteEventoBoton(Pizarra panel) {

        this.panel = panel;

    }

    public void dibujarFiguras() {
        for (int i = 0; i < panel.aFiguras.size(); i++) {
            Figura f = panel.aFiguras.get(i);

            if (f instanceof Linea) {
                
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
            }
            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
            }
            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntoC, (Point) cr.puntoR, cr.color);
            }
            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, t.color, false);
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
                panel.dibujarElipse((Point) el.puntoC, (Point) el.puntoR, (Point) el.puntoA, el.color);
            }
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton origen = (JButton) e.getSource();

        if (origen.getName().equals("escalar")) {

            System.out.println("Escalar");
            int n = Integer.parseInt(panel.textoNumeroFigura.getText());
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int esc = Integer.parseInt(panel.textoEscalar.getText());

                transX = ((int) l.punto1.getX() - panel.oX) * (-1);
                transY = ((int) l.punto1.getY() - panel.oY) * (-1);

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = l.punto1.getX() - panel.oX;
                punto[1] = l.punto1.getY() - panel.oY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = l.punto2.getX() - panel.oX;
                punto[1] = l.punto2.getY() - panel.oY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                l.punto2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                //panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                dibujarFiguras();

            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, Color.WHITE, false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int esc = Integer.parseInt(panel.textoEscalar.getText());

                transX = ((int) p1.x - panel.oX) * (-1);
                transY = ((int) p1.y - panel.oY) * (-1);

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = p1.getX() - panel.oX;
                punto[1] = p1.getY() - panel.oY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = p2.getX() - panel.oX;
                punto[1] = p2.getY() - panel.oY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                p2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = p3.getX() - panel.oX;
                punto[1] = p3.getY() - panel.oY;
                punto[2] = 1;
                m2.escalar(esc);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                p3.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                Vista v1 = new Vista(p1.x, p1.y, t.color.getRGB());
                Vista v2 = new Vista(p2.x, p2.y, t.color.getRGB());
                Vista v3 = new Vista(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //     panel.dibujarTriangulo(punto1, punto2, punto3, t.color, false);
                dibujarFiguras();
            }

            if (f instanceof Circulo) {
                System.out.println("Circulo");
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntoC, (Point) cr.puntoR, Color.WHITE);
                System.out.println(cr.puntoC.getX() + "," + cr.puntoR.getX());
                /*  double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int esc = Integer.parseInt(panel.textoEscalar.getText());
                

                transX = ((int) cr.puntoC.getX() - panel.oX) * (-1);
                transY = ((int) cr.puntoC.getY() - panel.oY) * (-1);
                
                punto[0] = cr.puntoC.getX();
                punto[1] = cr.puntoC.getY();
                punto[2] = 1;
                
                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = cr.puntoC.getX() - panel.oX;
                punto[1] = cr.puntoC.getY() - panel.oY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX*(-1),transY*(-1));
                punto2=m.pprima(punto);
                cr.puntoC.setLocation(punto2[0]+panel.oX, punto2[1]+panel.oY);

                punto[0] = cr.puntoR.getX();
                punto[1] = cr.puntoR.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = cr.puntoR.getX() - panel.oX;
                punto[1] = cr.puntoR.getY() - panel.oY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                cr.puntoR.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                panel.dibujarCirculo((Point) cr.puntoC, (Point) cr.puntoR, Color.RED); */
            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int esc = Integer.parseInt(panel.textoEscalar.getText());

                transX = ((int) cd.punto1.getX() - panel.oX) * (-1);
                transY = ((int) cd.punto1.getY() - panel.oY) * (-1);

                punto[0] = cd.punto1.getX();
                punto[1] = cd.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = cd.punto1.getX() - panel.oX;
                punto[1] = cd.punto1.getY() - panel.oY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                cd.punto1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = cd.punto2.getX() - panel.oX;
                punto[1] = cd.punto2.getY() - panel.oY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                cd.punto2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                //    panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                dibujarFiguras();
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
            }

        } else if (origen.getName().equals("rotar") && panel.textoNumeroFigura.getText() != null) {

            System.out.println("Rotar");
            int n = Integer.parseInt(panel.textoNumeroFigura.getText());
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int rotar = Integer.parseInt(panel.textoRotar.getText());

                transX = ((int) l.punto1.getX() - panel.oX) * (-1);
                transY = ((int) l.punto1.getY() - panel.oY) * (-1);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = l.punto2.getX() - panel.oX;
                punto[1] = l.punto2.getY() - panel.oY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                l.punto2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                //    panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, Color.WHITE, false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int rotar = Integer.parseInt(panel.textoRotar.getText());

                transX = ((int) p1.x - panel.oX) * (-1);
                transY = ((int) p1.y - panel.oY) * (-1);

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = p1.getX() - panel.oX;
                punto[1] = p1.getY() - panel.oY;
                punto[2] = 1;
                m.rotacion(rotar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = p2.getX() - panel.oX;
                punto[1] = p2.getY() - panel.oY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                p2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = p3.getX() - panel.oX;
                punto[1] = p3.getY() - panel.oY;
                punto[2] = 1;
                m2.rotacion(rotar);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                p3.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                Vista v1 = new Vista(p1.x, p1.y, t.color.getRGB());
                Vista v2 = new Vista(p2.x, p2.y, t.color.getRGB());
                Vista v3 = new Vista(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //    panel.dibujarTriangulo(punto1, punto2, punto3, t.color, false);
                dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;

            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int rotar = Integer.parseInt(panel.textoRotar.getText());

                transX = ((int) cd.punto1.getX() - panel.oX) * (-1);
                transY = ((int) cd.punto1.getY() - panel.oY) * (-1);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = cd.punto2.getX() - panel.oX;
                punto[1] = cd.punto2.getY() - panel.oY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                cd.punto2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                //    panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                dibujarFiguras();
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
            }

        } else {

            System.out.println("Trasladar");
            int n = Integer.parseInt(panel.textoNumeroFigura.getText());
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                l.punto2.setLocation(punto2[0], punto2[1]);

                //  panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, Color.WHITE, false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0], punto2[1]);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p2.setLocation(punto2[0], punto2[1]);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p3.setLocation(punto2[0], punto2[1]);

                Vista v1 = new Vista(p1.x, p1.y, t.color.getRGB());
                Vista v2 = new Vista(p2.x, p2.y, t.color.getRGB());
                Vista v3 = new Vista(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //  panel.dibujarTriangulo(punto1, punto2, punto3, t.color, false);
                dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;

            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                punto[0] = cd.punto1.getX();
                punto[1] = cd.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                cd.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                cd.punto2.setLocation(punto2[0], punto2[1]);

                //  panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                dibujarFiguras();
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
            }

        }
    }

}
