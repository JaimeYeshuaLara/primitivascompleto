
public class Vista {
    public int x, y;              // coordenada 
    public int argb;                // color 
    
    public Vista(int xvalor, int yvalor, int cvalor)
    {
        x = xvalor;
        y = yvalor;
        argb = cvalor;
    }
}
